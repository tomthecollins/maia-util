maia-util can be used to segment and label chords in given musical content. It
implements the forwards HarmAn algorithm (Pardo & Birmingham, 2002).

## Example

Suppose we have two bars of music in common time &ndash; the first containing
pitches mainly from the A-minor triad and the second mainly from the F-major
triad. The point set `ps` below represents these notes, with first dimension of
ontime, second dimension of MIDI note number, and third dimension of duration.

The notes can be segmented and labelled using the function
{@link harman_forward}. The `name` property of the output variable `lbl`'s first
element is the string "A minor", and the `name` property of the output variable
`lbl`'s second element is "F major".

```javascript
var mu = require('maia-util');
ps = [
  [0, 45, 51, 4, 0], [0, 72, 67, 4, 1], [0, 76, 69, 4, 1], [0.5, 52, 55, 3.5, 0], [1, 59, 59, 0.5, 0], [1.5, 60, 60, 2.5, 0],
  [4, 41, 49, 4, 0], [4, 72, 67, 4, 1], [4, 77, 70, 4, 1], [4.5, 48, 53, 3.5, 0], [5, 55, 57, 0.5, 0], [5.5, 57, 58, 2.5, 0]
];
var seg = mu.segment(ps);
var lbl = mu.harman_forward(
  seg,
  mu.chord_templates_pbmin7ths,
  mu.chord_lookup_pbmin7ths
);
console.log("lbl:", lbl);
```

For interactive versions, see this
[JSFiddle example](https://jsfiddle.net/maia_util/gjLst6kn/)
or this
[RunKit example](https://runkit.com/embed/pu4bwo8pmqsw).

## References

* Pardo, B., & Birmingham, W. P. (2002). Algorithms for chordal analysis.
*Computer Music Journal, 26*(2), 27-49.
