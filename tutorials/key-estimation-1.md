maia-util can be used to provide estimates of the key of music content. It
implements the Krumhansl-Schmuckler key-finding algorithm (Krumhansl, 1990).

## Example

Suppose we have the crotchet notes A4, C5, F4, followed by quavers E4, G#4. The
key estimate should be A minor. The point set `ps` below represents these notes,
with first dimension of ontime, second dimension of MIDI note number, and third
dimension of duration.

The key of the associated notes can be estimated using the function
{@link fifth_steps_mode}. The first element of the output variable `fsm` is the
string "A minor".

```javascript
var mu = require('maia-util');
var ps = [[0, 69, 1], [1, 72, 1], [2, 65, 1], [3, 64, 0.5], [3.5, 68, 0.5]];
var fsm = mu.fifth_steps_mode(
  ps,
  mu.krumhansl_and_kessler_key_profiles
);
console.log("fsm:", fsm);
```

For interactive versions, see this
[JSFiddle example](https://jsfiddle.net/maia_util/vogfeskp/)
or this
[RunKit example](https://runkit.com/embed/8l5m5gkwleph).

## References

* Krumhansl, C. L. (1990). *Cognitive foundations of musical
pitch*. Oxford University Press.
